import requests
import twitter
import os

# Tokens for Twitter and Slack are exported to Python virtualenv

SLACK_BOT_TOKEN="xoxb-63574561264-WFtbOkkZqsvch4No0OfX0eg4"

ACCESS_TOKEN="162587129-4u1JOgBfeVrhcGBtbn1oOodPitCx1SbGfp0Ogclb"
ACCESS_TOKEN_SECRET="1fPMmCV6Gz3bYU8kggQ8LAmyKXgKkF1aqDpEnDyeWLMZ9"
CONSUMER_KEY="BxrW45av4VQDCfc4IA32GO3QW"
CONSUMER_SECRET="ff6x1N1jvcW6V7yfuvb7XmNPYEvwq0hKviACeyQb5SipcrP4sG"
# SLACK_BOT_TOKEN = os.environ.get('SLACK_BOT_TOKEN')
# ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN')
# ACCESS_TOKEN_SECRET = os.environ.get('ACCESS_TOKEN_SECRET')
# CONSUMER_KEY = os.environ.get('CONSUMER_KEY')
# CONSUMER_SECRET = os.environ.get('CONSUMER_SECRET')


woeid = 1199682 # Quezon City
message = ""
channel = "#python_practice1"

def request_auth(cons_key, cons_secret, acc_key, acc_secret):
	return twitter.Api(cons_key, cons_secret, acc_key, acc_secret)

def request_trends(api, woeid):
	return api.GetTrendsWoeid(woeid)

def parse_trends(lstTrends):
	message = " Top 10 Trends today:\n"
	for i in range(0, 10):
		a = lstTrends[i].AsDict()
		message = message + "%i. %s\n" % (i+1, a['name'])
	return message

def post_payload(message, channel):
	url = "https://slack.com/api/chat.postMessage"
	payload = {
		"token": SLACK_BOT_TOKEN,
		"as_user": True,
		"channel": channel,
		"text": message
	}
	r = requests.post(url, data=payload)

def main():
	api = request_auth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
	trends = request_trends(api, woeid)
	message = parse_trends(trends)
	post_payload(message, channel)

main()

