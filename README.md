## Python Practice Bot

Slack bot which posts Top 10 Twitter Trending into a channel.

## Requirements
1. `requests`
2. `python-twitter`

## Set-up
Optional: Set up a `virtualenv` for the project

- Install requirements using `pip install` or using `requirements.txt`
- `export` or put in `~/.bashrc` your Slack bot token and Twitter access and consumer tokens to the following variables:

> `SLACK_BOT_TOKEN`

> `ACCESS_TOKEN`

> `ACCESS_TOKEN_SECRET`

> `CONSUMER_KEY`

> `CONSUMER_SECRET`

- Run the application

## Credits
- Christian Bermejo - developer
- bear - [python-twitter API](https://github.com/bear/python-twitter)
- kennethreitz - [requests](https://github.com/kennethreitz/requests)

## Misc
One can use their preferred web servers to schedule daily posts, for example. 

`PythonAnywhere` was used for this project.